<?php

/**
 * Date: 26/02/16
 * Time: 00:52
 */
namespace db;

use App\db_configuration;

class DbConnect
{
    const FIRST_BINDED_PARAMETER        = 1;
    const SECOND_BINDED_PARAMETER       = 2;
    private $pdo_connection;

    public function __construct()
    {
        try {
            $connection_configuration = "mysql:host=". DB_HOST .";dbname=". DB_NAME;
            $this->pdo_connection = new \PDO($connection_configuration, DB_USER, DB_PASSWORD);
        } catch (PDOException $exception) {
            return ("Connection error: " . $exception->getMessage());
        }
    }

    // Get all elements from table name
    public function getAllColumnsFromTable($table_name)
    {
        try {
            $result = $this->pdo_connection->query("SELECT * FROM $table_name");
            $result_table = $result->fetchAll(\PDO::FETCH_ASSOC);
            return $result_table;
        } catch (\PDOException $exception) {
            return ("DB error: " . $exception->getMessage());
        }
    }

    public function getTableColumnsFromId($table_name, $id)
    {
        try {
            $result = $this->pdo_connection->query("SELECT * FROM $table_name WHERE id=$id");
            $result_table = $result->fetchAll(\PDO::FETCH_ASSOC);
            return $result_table;
        } catch (\PDOException $exception) {
            return ("DB error: " . $exception->getMessage());
        }
    }

}
