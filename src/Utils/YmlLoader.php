<?php
/**
 * Date: 18/06/16
 * Time: 15:41
 */

namespace Utils;


class YmlLoader
{
    private $yml_path;
    protected $yml_file_content_array;

    public function __construct($yml_path)
    {
        $this->yml_path = $yml_path;
        $this->getYmlFileContent();
    }

    private function getYmlFileContent()
    {
        try {
            $this->yml_file_content_array = yaml_parse_file($this->yml_path);
        } catch (\Exception $e) {
            echo "Yaml exception: ", $e->getMessage();
            return;
        }
    }
}