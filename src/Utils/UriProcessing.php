<?php

namespace Utils;

use Myfirstframework\Request;

/**
 * Date: 26/02/16
 * Time: 16:53
 */
class UriProcessing
{
    const FIRST_POSITION_REQUEST_URI_ARRAY_EMPTY_TO_REMOVE  = 0;
    const CONTROLLER_NAME_POSITION_URI                      = 1;
    const FUNCTION_NAME_POSITION_URI                        = 2;
    const PARAMETERS_POSITION_URI                           = 3;
    const SEPARATOR                                         = '/';

    private $request;
    private $controller_name;
    private $function_name;
    private $controller_parameters_array;
    private $brokendown_uri_array;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->controller_parameters_array  = array();
        $this->brokendown_uri_array         = array();

        $this->breakDownUri();
        $this->setControllerName();
        $this->buildControllerFunctionsArray();
    }

    public function getControllerName()
    {
        return $this->controller_name;
    }

    public function getControllerFunctionName()
    {
        return $this->function_name;
    }

    public function getControllerParametersArray()
    {
        return $this->controller_parameters_array;
    }

    public function getTotalNumberParameters()
    {
        return count($this->controller_parameters_array);
    }

    public function getParameters()
    {
        return $this->controller_parameters_array;
    }

    private function setControllerName()
    {
        $this->controller_name = $this->brokendown_uri_array[self::CONTROLLER_NAME_POSITION_URI];
    }

    private function breakDownUri()
    {
        $this->brokendown_uri_array = $this->breakDownArray($this->request->getRequestUri());
        unset($this->brokendown_uri_array[self::FIRST_POSITION_REQUEST_URI_ARRAY_EMPTY_TO_REMOVE]);
    }

    private function breakDownArray($array)
    {
        return explode(self::SEPARATOR, $array);
    }

    private function buildControllerFunctionsArray()
    {
        $this->function_name = $this->brokendown_uri_array[self::FUNCTION_NAME_POSITION_URI];

        for($parameters_index = self::PARAMETERS_POSITION_URI; $parameters_index <= count($this->brokendown_uri_array); $parameters_index++)
        {
            array_push($this->controller_parameters_array, $this->brokendown_uri_array[$parameters_index]);
        }
    }

}