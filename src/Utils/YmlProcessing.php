<?php
/**
 * Date: 26/02/16
 * Time: 17:47
 */

namespace Utils;

class YmlProcessing
{
    const ROUTING_FILE                          = 'routing.yml';
    const DOESNT_EXISTS_ERROR_MESSAGE           = "ERROR: Controller name is not defined on routing.yml file.";
    const FUNCTION_DOESNT_EXISTS_MESSAGE        = "ERROR: This function is not defined on routing.yml file.";
    const TOO_MUCH_PARAMETERS_ERROR_MESSAGE     = "ERROR: There are too much parameters on uri than are defined on routing.yml file.";
    const MISSING_PARAMETERS_ERROR_MESSAGE      = "ERROR: There are missing parameters on uri than are defined on routing.yml file.";

    private $route_yml_path;
    private $yml_file_content_array;
    private $uri_processing;

    public function __construct(UriProcessing $uriProcessing)
    {
        $this->uri_processing = $uriProcessing;
        $this->route_yml_path = $_SESSION['app_path'] . self::ROUTING_FILE;
        $this->getYmlFileContent();

//        var_dump(array('YML' => $this->yml_file_content_array, 'SESSION' => $_SESSION));
    }

    private function getYmlFileContent()
    {
        try {
            $this->yml_file_content_array = yaml_parse_file($this->route_yml_path);
        } catch (\Exception $e) {
            echo "Yaml exception: ", $e->getMessage();
            return;
        }
    }

    public function controllerRequestDefinedOnRoutingYml()
    {
        foreach($this->yml_file_content_array as $controller_name_yml_file => $yml_content_array) {
            // Check controller name exists
            if($controller_name_yml_file === $this->uri_processing->getControllerName()) {
                return $this->controllerFunctionExists($yml_content_array);
            }
        }

        $_SESSION['error'] = self::DOESNT_EXISTS_ERROR_MESSAGE;
        return false;
    }

    private function controllerFunctionExists($yml_content_array)
    {
        for($i=0; $i<=count($yml_content_array); $i++) {
            foreach($yml_content_array[$i] as $function_yml_file => $parameters_yml_file) {
                if($function_yml_file === $this->uri_processing->getControllerFunctionName()) {
                    return $this->checkParametersNeedForFunction($parameters_yml_file);
                }
            }
        }

        $_SESSION['error'] = self::FUNCTION_DOESNT_EXISTS_MESSAGE;
        return false;
    }

    private function checkParametersNeedForFunction($parameters_yml_file)
    {
        $count_parameters_yml_file  = count($parameters_yml_file);
        $count_parameters_uri       = $this->uri_processing->getTotalNumberParameters();

        switch(true)
        {
            case($count_parameters_uri > $count_parameters_yml_file):
                $_SESSION['error'] = self::TOO_MUCH_PARAMETERS_ERROR_MESSAGE;
                return false;
                break;
            case($count_parameters_uri < $count_parameters_yml_file):
                $_SESSION['error'] = self::MISSING_PARAMETERS_ERROR_MESSAGE;
                return false;
                break;
            case($count_parameters_yml_file === $count_parameters_uri):
                return true;
                break;
        }
    }

}