<?php
/**
 * Date: 18/06/16
 * Time: 15:51
 */

namespace Utils;

class YmlServiceProcessing extends YmlLoader
{
    const ARGUMENTS_POSITION        = 0;
    const PUBLIC_POSITION           = 1;
    const TAGS_POSITION             = 2;

    const MISSING_DEFINED_FILE      = 'Error: Resource is not defined on yml service file: ';


    private $all_defined_controllers_and_services;

    public function __construct($yml_path)
    {
        parent::__construct($yml_path);
        $this->loadAllDefinedControllersAndServicesDefinedOnYmlFile();
    }

    public function getArgumentsFromPathControllerOrService($controller_service_path)
    {
        return explode(',', $this->yml_file_content_array[$controller_service_path][self::ARGUMENTS_POSITION]['arguments']);
    }

    public function isPublicService($service_path)
    {
        return explode('/', $this->yml_file_content_array[$service_path][self::PUBLIC_POSITION]['public'])[0] == 1;
    }

    public function getServiceTags($service_path)
    {
        return explode('/', $this->yml_file_content_array[$service_path][self::TAGS_POSITION]['tags'])[0];
    }

    public function serviceOrControllerIsDefined($service_controller_path_name)
    {
        return in_array($service_controller_path_name, $this->all_defined_controllers_and_services);
    }

    public function getAllDefinedControllersAndServices()
    {
        return $this->all_defined_controllers_and_services;
    }

    public function getControllerOrServiceName($controller_service)
    {
        return explode('/', $controller_service)[1];
    }

    public function isServiceFromPath($element_path)
    {
        return 'Services' === explode('/', $element_path)[0];
    }

    private function loadAllDefinedControllersAndServicesDefinedOnYmlFile()
    {
        $this->all_defined_controllers_and_services = array();

        foreach($this->yml_file_content_array as $to_require => $to_require_content) {
            array_push($this->all_defined_controllers_and_services, $to_require);
        }
    }

}