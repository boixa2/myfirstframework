<?php
/**
 * Date: 18/06/16
 * Time: 19:34
 */

namespace Myfirstframework;

use Utils\YmlServiceProcessing;

class DependencyInjectionContainer
{
    const DEPENDENCY_FILE_ERROR             = "Error: File doesn't exists: ";
    const RESOURCE_NOT_DEFINED              = "Error: This resource is not defined on service yml file: ";
    const RESOURCE_NOT_FOUND                = "Error: Resource not found: ";
    const FILE_NOT_EXISTS                   = "Error: File doesn't exist on src directory: ";
    const RESOURCE_BAD_DEFINED              = "Error: This resource is not defined correctly on yml service file: ";
    const DEPENDENCY_CLASS_NAME_POSITION    = 1;

    private $yml_service;
    private $request;
    private $controller_name_reference;
    private $controller_reference_path;
    private $controller_arguments_array;

    private $array_object_dependencies;
    private $services_definition;
    private $public_services;

    public function __construct(Request $request, YmlServiceProcessing $yml_service)
    {
        $this->request                          = $request;
        $this->yml_service                      = $yml_service;
        $this->controller_arguments_array       = array();
        $this->array_object_dependencies        = array();
        $this->services_definition              = array();
        $this->public_services                  = array();
    }

    public function getSpecifiedServiceOrController($controller_service_path)
    {
        foreach($this->array_object_dependencies as $object_path => $object) {
            if($controller_service_path === $object_path) return $object;
        }

        $this->request->saveSession('error', self::RESOURCE_NOT_FOUND . $controller_service_path);
        return false;
    }

    public function controllerIsDefined($service_controller_name)
    {
        $this->controller_reference_path = "Controllers/$service_controller_name";

        if(!$this->fileExists($this->controller_reference_path) ||
           !$this->serviceOrControllerDefinedOnYml($this->controller_reference_path)) return false;

        $this->controller_name_reference = $service_controller_name;
        return true;
    }

    public function instanceAllDefinedDependencies()
    {
        $all_yml_elements_defined = $this->yml_service->getAllDefinedControllersAndServices();

        for($i=0; $i<count($all_yml_elements_defined); $i++) {
            if(!$this->fileExists($all_yml_elements_defined[$i]) ||
               !$this->serviceOrControllerDefinedOnYml($all_yml_elements_defined[$i])) return false;

            $this->saveAttributesOnServiceCase($all_yml_elements_defined[$i]);

            $element_dependencies_array = $this->yml_service->getArgumentsFromPathControllerOrService($all_yml_elements_defined[$i]);

            if($element_dependencies_array[0] === '') {
                $this->createAndSaveDefinedInstancies($all_yml_elements_defined[$i]);
            } else {
                $arguments_required = $this->loadElementsAlreadyInstanced($element_dependencies_array);
                if($arguments_required === false) return false;
                $this->createAndSaveDefinedInstancies($all_yml_elements_defined[$i], $arguments_required);
            }
        }

        return true;
    }

    public function getPublicServices()
    {
        foreach($this->services_definition as $service_path => $service_info) {
            if($service_info['public']) {
                $this->public_services[$service_path] = $this->array_object_dependencies[$service_path];
            }
        }
        return $this->public_services;
    }

    private function saveAttributesOnServiceCase($service_path)
    {
        if($this->yml_service->isServiceFromPath($service_path)) {
            $this->services_definition[$service_path]['public'] = $this->yml_service->isPublicService($service_path);
            $this->services_definition[$service_path]['tags']   = $this->yml_service->getServiceTags($service_path);
        }
    }

    private function createAndSaveDefinedInstancies($class_path, $array_objects_dependency = array()) {

        require_once $this->request->getSourcePath() . $class_path . '.php';

        $class_name = $this->yml_service->getControllerOrServiceName($class_path);

        if($this->yml_service->isServiceFromPath($class_path)) {
            $new_object = new $class_name($this->request, $array_objects_dependency, $this->services_definition[$class_path]);
        } else {
            $new_object = new $class_name($this->request, $array_objects_dependency);
        }

        $this->array_object_dependencies[$class_path] = $new_object;
    }

    private function loadElementsAlreadyInstanced($elements_array_to_check)
    {
        $required_dependencies = array();

        for($i=0; $i<count($elements_array_to_check); $i++) {

            if(!array_key_exists($elements_array_to_check[$i], $this->array_object_dependencies)) {
                $this->request->saveSession('error', self::RESOURCE_BAD_DEFINED . $elements_array_to_check[$i]);
                return false;
            }

            $required_dependencies[$elements_array_to_check[$i]] = $this->array_object_dependencies[$elements_array_to_check[$i]];
        }

        return $required_dependencies;
    }

    private function fileExists($file_src_path_to_check)
    {
        if(!file_exists($this->request->getSourcePath() . $file_src_path_to_check . '.php')) {
            $this->request->saveSession('error', self::FILE_NOT_EXISTS . $file_src_path_to_check . '.php');
            return false;
        }
        return true;
    }

    private function serviceOrControllerDefinedOnYml($service_controller_to_check)
    {
        if(!$this->yml_service->serviceOrControllerIsDefined($service_controller_to_check)) {
            $this->request->saveSession('error', self::RESOURCE_NOT_DEFINED . $service_controller_to_check);
            return false;
        }

        return true;
    }

}