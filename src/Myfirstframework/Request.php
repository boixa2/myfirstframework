<?php
/**
 * Date: 25/02/16
 * Time: 17:38
 */

namespace Myfirstframework;

class Request
{
    const DEV_SERVER_NAME_NOT_DEFINED_ERROR     = "Error: Constant 'DEV_SERVER_NAME' is not configured on framework configuration file.";
    const PROD_SERVER_NAME_NOT_DEFINED_ERROR    = "Error: Constant 'PROD_SERVER_NAME' is not configured on framework configuration file.";

    private $get;
    private $post;
    private $cookies;
    private $server;

    function __construct()
    {
        $this->get          = $_GET;
        $this->post         = $_POST;
        $this->cookies      = $_COOKIE;
        $this->server       = $_SERVER;
        session_start();
    }

    public function isDevelopmentMode()
    {
        if(!defined('DEV_SERVER_NAME')) {
            $_SESSION['error'] = self::DEV_SERVER_NAME_NOT_DEFINED_ERROR;
            return null;
        }
        return DEV_SERVER_NAME === $this->getServerName();
    }

    public function isProductionMode()
    {
        if(!defined('PROD_SERVER_NAME')) {
            $_SESSION['error'] = self::PROD_SERVER_NAME_NOT_DEFINED_ERROR;
            return null;
        }
        return PROD_SERVER_NAME === $this->getServerName();
    }

    public function saveSession($session_name, $session_value)
    {
        $_SESSION[$session_name] = $session_value;
    }

    public function getSession($session_name)
    {
        return $_SESSION[$session_name];
    }

    public function getRoutingYmlPath()
    {
        return $this->server['DOCUMENT_ROOT'] . 'app/routing.yml';
    }

    public function getProductionServiceYmlPath()
    {
        return $this->server['DOCUMENT_ROOT'] . 'app/service.yml';
    }

    public function getDevelopmentServiceYmlPath()
    {
        return $this->server['DOCUMENT_ROOT'] . 'app/service_dev.yml';
    }

    public function getSourcePath()
    {
        return $this->server['DOCUMENT_ROOT'] . 'src/';
    }

    public function getControllersPath()
    {
        return $this->server['DOCUMENT_ROOT'] . 'src/Controllers/';
    }

    public function getTwigTemplatePath()
    {
        return $this->server['DOCUMENT_ROOT'] . TWIG_TEMPLATE_PATH;
    }

    public function getSmartyTemplatePath()
    {
        return $this->server['DOCUMENT_ROOT'] . SMARTY_TEMPLATE_PATH;
    }

    public function getSmartyCompileDirectoryPath()
    {
        return $this->getSmartyTemplatePath() . 'templates_c/';
    }

    public function getSmartyConfigPath()
    {
        return $this->getSmartyTemplatePath() . 'configs/';
    }

    public function getSmartyCachePath()
    {
        return $this->getSmartyTemplatePath() . 'cache/';
    }

    public function getRequestUri()
    {
        return $this->server['REQUEST_URI'];
    }

    public function getFrameworkConfigurationFile()
    {
        return $this->server['DOCUMENT_ROOT'] . 'app/framework_config.php';
    }

    public function getServiceConfigurationFile()
    {
        return $this->server['DOCUMENT_ROOT'] . 'app/service.yml';
    }

    public function errorExists()
    {
        return isset($_SESSION['error']);
    }

    public function showError()
    {
        echo $_SESSION['error'];
    }

    public function deleteErrors()
    {
        unset($_SESSION['error']);
    }

    private function getServerName()
    {
        return $this->server['SERVER_NAME'];
    }

}