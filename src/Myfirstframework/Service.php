<?php
/**
 * Date: 08/03/16
 * Time: 18:01
 */

namespace Myfirstframework;

use db\DbConnect;

class Service
{
    private $dependencies;
    private $pdo;
    private $request;
    private $is_public;
    private $tags;

    public function __construct(Request $request, $service_dependencies = array(), $service_definition)
    {
        $this->pdo          = new DbConnect();
        $this->request      = $request;
        $this->dependencies = $service_dependencies;
        $this->is_public    = $service_definition['public'];
        $this->tags         = $service_definition['tags'];
    }

    public function getAllRecordsFromTable($table_name)
    {
        return $this->pdo->getAllColumnsFromTable($table_name);
    }

    public function getRecordsFromId($table_name, $id)
    {
        return $this->pdo->getTableColumnsFromId($table_name, $id);
    }

    public function isPublic()
    {
        return $this->is_public;
    }

    public function getTags()
    {
        return $this->tags;
    }

}