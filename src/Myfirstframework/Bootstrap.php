<?php
namespace Myfirstframework;

use Myfirstframework\Routing;
use Utils\YmlServiceProcessing;

class Bootstrap
{
    const CONFIGURATION_FILE_NOT_EXISTS_ERROR   = 'Error: Framework configuration file doesn\'t exists';
    const TEMPLATE_PATH_ERROR                   = 'Error: No Twig or Smarty template path are defined on framework configuration: Define to fix it.';
    const DEV_MODE_NEED_ERROR                   = "Error: Framework needs a dev mode configuration. Fix it adding the constant 'DEV_SERVER_NAME' on your framework_config file.";

    private $routing;
    private $controller;
    private $request;
    private $yml_service;
    private $dependency_injection;
    private $public_services;

    public function __construct(Request $request)
    {
        $this->request          = $request;
        $this->public_services  = array();
        $this->routing          = new Routing($this->request);

        if(!$this->missingFrameworkRequirements()) {
            $this->yml_service = new YmlServiceProcessing($this->getYmlPathAccordingDevMode());
            $this->upControllerAndItsDependencies();
        }
        $this->onDevModeShowErrorsIfExist();
    }

    public function getPublicService($path_service_name)
    {

        return $this->public_services[$path_service_name];
    }

    private function onDevModeShowErrorsIfExist()
    {
        if(!$this->devModeIsCorrectlyConfigured() || $this->request->isDevelopmentMode() && $this->request->errorExists()) {
            $this->request->showError();
            $this->request->deleteErrors();
        }
    }

    private function devModeIsCorrectlyConfigured()
    {
        if($this->request->isDevelopmentMode() === null) {
            $this->request->saveSession('error', self::DEV_MODE_NEED_ERROR);
            return false;
        }
        return true;
    }

    private function onDevEnableShowServerErrors()
    {
        /** DEV Mode: Show errors */
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    private function missingFrameworkRequirements()
    {
        if(!$this->frameworkConfigurationFileExists()) return true;
        $this->loadFrameworkConfigurationFile();
        if($this->request->isProductionMode() === null || $this->request->isProductionMode() === null) return true;
        $this->onDevEnableShowServerErrors();
        if(!$this->routing->controllerExistsUriYml()) return true;
        if($this->twigOrSmartyDirectoryFilesDoesntExist()) return true;
    }

    private function upControllerAndItsDependencies()
    {
        $controller_name            = $this->routing->getControllerName();
        $controller_function        = $this->routing->getControllerFunctionName();

        $this->dependency_injection = new DependencyInjectionContainer( $this->request,
                                                                        $this->yml_service);

        if(!$this->dependency_injection->controllerIsDefined($controller_name) ||
           !$this->dependency_injection->instanceAllDefinedDependencies()) return;
        $this->public_services = $this->dependency_injection->getPublicServices();

        $this->controller = $this->dependency_injection->getSpecifiedServiceOrController("Controllers/$controller_name");
        $this->controller->$controller_function($this->routing->getControllerParameters());

    }

    private function frameworkConfigurationFileExists()
    {
        if(!file_exists($this->request->getFrameworkConfigurationFile())) {
            $this->request->saveSession('error', self::CONFIGURATION_FILE_NOT_EXISTS_ERROR);
            return false;
        }
        return true;
    }

    private function loadFrameworkConfigurationFile()
    {
        require_once $this->request->getFrameworkConfigurationFile();
    }

    private function twigOrSmartyDirectoryFilesDoesntExist()
    {
        if(!defined('TWIG_TEMPLATE_PATH') || !defined('SMARTY_TEMPLATE_PATH')) {
            $this->request->saveSession('error', self::TEMPLATE_PATH_ERROR);
            return true;
        }
        return false;
    }

    private function getYmlPathAccordingDevMode()
    {
        return  $this->request->isDevelopmentMode() ?
                $this->request->getDevelopmentServiceYmlPath() :
                $this->request->getProductionServiceYmlPath();
    }
}
