<?php
/**
 * Date: 29/02/16
 * Time: 01:13
 */

namespace Myfirstframework;


class Response
{
    private $http;
    private $json;
    private $response;

    public function __construct($query_array)
    {
        $this->response = $query_array;
        $this->json = self::createJsonResponse($query_array[0]);
    }

    public function getResponseArray()
    {
        $this->setContentTypeHeader('text/html');
        return $this->response;
    }

    public function getJson()
    {
        $this->setContentTypeHeader('application/json');
        return $this->json;
    }

    private function createJsonResponse($controllers_return_array)
    {
        return json_encode($controllers_return_array);
    }

    private function setContentTypeHeader($header_value)
    {
        header("Content-Type: " . $header_value);
    }
}