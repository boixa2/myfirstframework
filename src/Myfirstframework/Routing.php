<?php
/**
 * Date: 25/02/16
 * Time: 18:29
 */

namespace Myfirstframework;

use Utils\UriProcessing;
use Utils\YmlRoutingProcessing;

class Routing
{

    private $request;
    private $uri_processing;
    private $yml_processing;

    public function __construct(Request $request)
    {
        $this->request          = $request;
        $this->uri_processing   = new UriProcessing($this->request);
        $this->yml_processing   = new YmlRoutingProcessing($this->uri_processing, $this->request->getRoutingYmlPath());
    }

    public function controllerExistsUriYml()
    {
        return $this->yml_processing->controllerRequestDefinedOnRoutingYml();
    }

    public function getControllerName()
    {
       return $this->uri_processing->getControllerName();
    }

    public function getControllerFunctionName()
    {
        return $this->uri_processing->getControllerFunctionName();
    }

    public function getControllerParameters()
    {
        return $this->uri_processing->getTotalNumberParameters() > 0 ? $this->uri_processing->getParameters() : array();
    }

}