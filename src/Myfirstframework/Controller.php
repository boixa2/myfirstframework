<?php
/**
 * Date: 26/02/16
 * Time: 23:52
 */

namespace Myfirstframework;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Smarty;

abstract class Controller
{
    const SMARTY_CONFIG_FILE_NAME = 'my_smarty.config';

    private $request;
    private $service;
    private $response;
    private $twig;
    private $smarty;
    private $title = 'MyTemplate';
    private $controller_dependencies_array;

    public function __construct(Request $request, $controller_dependencies)
    {
        $this->request = $request;
        $this->controller_dependencies_array = $controller_dependencies;
    }

    public function getDependenceByPathName($dependence_name)
    {
        return $this->controller_dependencies_array[$dependence_name];
    }

    public function prepareResponse($controllers_return_array)
    {
        $this->response = new Response($controllers_return_array);
    }

    public function getAll($db_table_name)
    {
        return $this->service->getAllRecordsFromTable($db_table_name);
    }

    public function getById($db_table_name, $id)
    {
        return $this->service->getRecordsFromId($db_table_name, $id);
    }

    public function getJsonResponse()
    {
        echo $this->response->getJson();
    }

    private function loadTwig()
    {
        $template_path 	= new Twig_Loader_Filesystem($this->request->getTwigTemplatePath());
        $this->twig		= new Twig_Environment($template_path);
    }

    private function loadSmarty()
    {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir($this->request->getSmartyTemplatePath());
        $this->smarty->setCompileDir($this->request->getSmartyCompileDirectoryPath());
        $this->smarty->setConfigDir($this->request->getSmartyConfigPath());
        $this->smarty->setCacheDir($this->request->getSmartyCachePath());
        $this->smarty->configLoad($this->request->getSmartyConfigPath() . self::SMARTY_CONFIG_FILE_NAME);
    }

    public function renderTwigTemplateFromResponse($twig_template_name, $additional_render_data = array())
    {
        $this->loadTwig();
        $twig_content = $this->twig->loadTemplate($twig_template_name);
        echo $twig_content->render(array('title' => $this->title, 'data' => $additional_render_data, 'json' => $this->response->getResponseArray()));
    }

    public function renderSmartyTemplateFromResponse($smarty_template_name, $additional_render_data = array())
    {
        $this->loadSmarty();
        $this->smarty->assign(array('title' => $this->title, 'data' => $additional_render_data, 'json' => $this->response->getResponseArray()));
        $this->smarty->display($smarty_template_name);
    }

    public function saveTitlePage($title)
    {
        $this->title = $title;
    }


}